<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class CategoryRepository extends EntityRepository
{
    public function findAllAndOrderByDate()
    {
        return $this->createQueryBuilder('c')
            ->orderBy('c.createdAt', 'asc')
            ->getQuery()
            ->getResult();
    }

    public function removeByName($categoryName)
    {
        $this->createQueryBuilder('c')
            ->delete()
            ->where('c.category = :category')
            ->setParameter(':category', $categoryName)
            ->getQuery()
            ->execute();
    }

    public function findByName($categoryName)
    {
        return $this->findOneBy(['category' => $categoryName]);
    }
}
