<?php

namespace AppBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class DateTransformer implements DataTransformerInterface
{
    public function transform($date)
    {
        return date('Y-m-d', $date->getTimestamp());
    }

    public function reverseTransform($value)
    {
        return (new \DateTime)->setTimestamp(strtotime($value));
    }
}