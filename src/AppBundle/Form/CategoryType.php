<?php

namespace AppBundle\Form;

use AppBundle\Entity\Category;
use AppBundle\Form\DataTransformer\DateTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CategoryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('category', TextType::class)
            ->add('description', TextType::class)
            
            ->add('createdAt', TextType::class,[
                'label' => 'date'
            ])
            ->add('save', SubmitType::class)
        ;
        $builder->get('createdAt')
            ->addModelTransformer(new DateTransformer());
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Category::class,
        ));
    }

    public function getBlockPrefix()
    {
        return 'app_category';
    }
}