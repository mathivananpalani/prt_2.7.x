<?php

namespace AppBundle\Service;

use AppBundle\Entity\Category as EntityCategory;
use Doctrine\ORM\EntityManagerInterface;

class Category
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function getCategories()
    {
        $categories = $this->em->getRepository(EntityCategory::class)
            ->findAllAndOrderByDate();

        return $this->formatCategory($categories); 
    }

    public function findCategory($categoryName)
    {
        $category = $this->em->getRepository(EntityCategory::class)
            ->findByName($categoryName);
        return $category;
    }

    public function remove($categoryName)
    {
        $this->em->getRepository(EntityCategory::class)
            ->removeByName($categoryName);
    }

    private function formatCategory(array $products)
    {
        $formated = [];

        foreach($products as $key => $value)
        {
            array_push($formated,[
                'name' => $value->getCategory(),
                'description' => $value->getDescription(),
                'date' => $value->getCreatedAt()
            ]);
        }
        return $formated;
    }
}
