<?php

namespace AppBundle\EventListener;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;

class RequestListener
{
    public function onKernelRequest(GetResponseEvent $event)
    {
        if(!$event->isMasterRequest()){
            return;
        }

        $request = $event->getRequest();
        if($request->query->get('trigger')) {
            $request->request->set('event', true);
        }
    }
}