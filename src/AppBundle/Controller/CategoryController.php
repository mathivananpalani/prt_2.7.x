<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Category;
use AppBundle\Event\Populate;
use AppBundle\Form\CategoryType;
use AppBundle\Service\Category as ServiceCategory;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class CategoryController extends Controller
{
    private $categoryService;

    public function __construct(ServiceCategory $categoryService)
    {
        $this->categoryService = $categoryService;
    }

    public function addAction(Request $request)
    {
        $categoryName = $request->get('categoryName', null);
        $em = $this->getDoctrine()->getManager();
        // $this->categoryService = $this->container->get('category_service');
        if ($categoryName) {
            $category = $this->categoryService->findCategory($categoryName);
        } else {
            $category = new Category();
        }
        $this->dispatcher = $this->container->get('event_dispatcher');
        $populate = new Populate($category);
        $this->dispatcher->dispatch(Populate::DATE_EVENT, $populate);

        $form = $this->createForm(CategoryType::class, $category);

        if ($request->getMethod() === 'POST') {
            $form->handleRequest($request);
        }

        if($form->isSubmitted() && $form->isValid()) {
            $category = $form->getData();
            $em->persist($category);
            $em->flush();
            $em->clear();

            return $this->redirectToRoute('add_category');
        }

        $categories = $this->categoryService->getCategories();

        return $this->render('product/category.html.twig', array(
            'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
            'form' => $form->createView(),
            'categories' => $categories,
            'edit' => isset($categoryName)
        ));
    }

    public function deleteAction($categoryName)
    {
        $this->categoryService->remove($categoryName);
        return $this->redirectToRoute('add_product');
    }
}