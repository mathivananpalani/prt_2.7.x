<?php

namespace AppBundle\Controller;

use DateTime;
use AppBundle\Entity\Product;
use AppBundle\Event\Populate;
use AppBundle\Form\ProductType;
use AppBundle\Service\Product as ServiceProduct;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ProductController extends Controller
{
    private $productService;

    public function __construct(ServiceProduct $productService)
    {
        $this->productService = $productService;
    }

    public function addAction(Request $request)
    {
        $productName = $request->get('productName', null);
        $em = $this->getDoctrine()->getManager();
        if ($productName) {
            $product = $this->productService->findProduct($productName);
        } else {
            $product = new Product();
        }
        
        $this->dispatcher = $this->container->get('event_dispatcher');
        $populate = new Populate($product);
        $this->dispatcher->dispatch(Populate::DATE_EVENT, $populate);

        $form = $this->createForm(ProductType::class, $product);

        if ($request->getMethod() === 'POST') {
            $form->handleRequest($request);
        }

        if($form->isSubmitted() && $form->isValid()) {
            $product = $form->getData();
            $em->persist($product);
            $em->flush();
            $em->clear();

            return $this->redirectToRoute('add_product');
        }

        $products = $this->productService->getProducts();

        return $this->render('product/product.html.twig', array(
            'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
            'form' => $form->createView(),
            'products' => $products,
            'edit' => isset($productName),
            'event' => $request->get('event')
        ));
    }

    public function deleteAction($productName)
    {
        $this->productService->remove($productName);
        return $this->redirectToRoute('add_product');
    }
}
