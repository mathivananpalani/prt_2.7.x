<?php

namespace AppBundle\Event;

use Symfony\Component\EventDispatcher\Event;

class Populate extends Event
{
    const DATE_EVENT = 'populate.date_event';
    private $instance;

    public function __construct($obj)
    {
        $this->instance = $obj;
    }

    public function getInstance()
    {
        return $this->instance;
    }
}